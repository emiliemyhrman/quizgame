//
//  main.m
//  QuizGame
//
//  Created by Emilie Myhrman on 2016-02-03.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
