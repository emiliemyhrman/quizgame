//
//  ViewController.m
//  QuizGame
//
//  Created by Emilie Myhrman on 2016-02-03.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UIButton *startGameButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


@end
