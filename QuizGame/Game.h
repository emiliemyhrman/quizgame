//
//  Game.h
//  QuizGame
//
//  Created by Emilie Myhrman on 2016-02-05.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Game : NSObject

- (NSString*)newQuestion;

@end
