//
//  QuizView.m
//  QuizGame
//
//  Created by Emilie Myhrman on 2016-02-07.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import "QuizView.h"
#import "Game.h"

@interface QuizView ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonA;
@property (weak, nonatomic) IBOutlet UIButton *buttonB;
@property (weak, nonatomic) IBOutlet UIButton *buttonC;
@property (weak, nonatomic) IBOutlet UIButton *buttonD;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestionButton;

@property (strong, nonatomic) Game *generateQuestion;

@end

@implementation QuizView

-(Game*)generateQuestion {
    if (!_generateQuestion) {
        _generateQuestion = [[Game alloc] init];
    }
    return _generateQuestion;
}


- (void)viewDidLoad {
    
    self.questionLabel.text = [self.generateQuestion newQuestion];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)answerA:(id)sender {
    NSLog(@"Answer A pressed");
    
}

- (IBAction)answerB:(id)sender {
    NSLog(@"Answer B pressed");
}

- (IBAction)answerC:(id)sender {
    NSLog(@"Answer C pressed");
}

- (IBAction)answerD:(id)sender {
    NSLog(@"Answer D pressed");
}

- (IBAction)nextQuestionButton:(id)sender {
    self.questionLabel.text = [self.generateQuestion newQuestion];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}*/


@end
