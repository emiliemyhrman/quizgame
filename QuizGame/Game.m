//
//  Game.m
//  QuizGame
//
//  Created by Emilie Myhrman on 2016-02-05.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//


#import "Game.h"
#import "QuizView.h"


@interface Game()
@property (nonatomic) NSMutableArray *questions;
//@property (nonatomic) NSArray *answers;
@property (nonatomic) NSString *newQuestion;

@end

@implementation Game

- (instancetype)init {
    self = [super init];
    if (self) {
        self.questions = [[NSMutableArray alloc]init];
        
        [_questions addObject:@"Fråga 1"];
        [_questions addObject:@"Fråga 2"];
        [_questions addObject:@"Fråga 3"];
        [_questions addObject:@"Fråga 4"];
        [_questions addObject:@"Fråga 5"];
        [_questions addObject:@"Fråga 6"];
        [_questions addObject:@"Fråga 7"];
        [_questions addObject:@"Fråga 8"];
        [_questions addObject:@"Fråga 9"];
        [_questions addObject:@"Fråga 10"];
        
            }
    return self;
}
   


- (NSString*)randomQuestion:(NSArray*)array {
    
    return array[arc4random() % array.count];
}


- (NSString*)newQuestion {
    
    NSString *question = [self randomQuestion:self.questions];
    
    return question;
}

@end
